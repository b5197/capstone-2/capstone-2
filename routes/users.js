//[SECTION] DEPENDENCIES AND MODULES
	const express = require('express');
	const controllers = require('../controllers/users');
	const auth = require('../auth');

//[SECTION] ROUTING COMPONENT
	const route = express.Router();

//[SECTION] POST USER REG
	route.post('/register', (req, res) => {
		/*console.log(req.body);*/
		let userInput = req.body;
		controllers.userReg(userInput).then(outcome => { res.send(outcome);
		});
	});

//[SECTION] USER AUTH
	route.post('/login', (req, res) => {
		controllers.loginUser(req.body).then(result => res.send(result));
	});

//[SECTION] ROUTES - GET
	route.get('/details', auth.verify, (req,res) => {
		controllers.getProfile(req.user.id).then(result => res.send(result));
	});

//[SECTION] CREATE ORDER
	route.post('/create-order', auth.verify, controllers.createOrder); 

	// route.post('/checkout', auth.verify, controllers.checkout);

//[SECTION] GET ORDERS
	route.get('/order-history', auth.verify, controllers.orderHistory);

//[SECTION]
	module.exports = route;

