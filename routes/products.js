//[SECTION] DEPENDENCIES AND MODULES
	const express = require('express');
	const route = express.Router();
	const ProductController = require('../controllers/products');
	const auth = require('../auth');
	const controllers = require('../controllers/products')

//[SECTION] DESTRUCTURE
	const {verify, verifyAdmin} = auth;

//[SECTION] CREATE PRODUCT
	route.post('/create', verify, verifyAdmin, (req, res) => {
		ProductController.addProduct(req.body).then(result => res.send(result));
	});

//[SECTION]GET ALL COURSES
	route.get('/all', (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});

//[SECTION] GET
	//Retrieve all ACTIVE products
	route.get('/active', (req, res) => {
		ProductController.getAllActive().then(result => res.send(result));
	});

	//RETRIEVE SINGLE PRODUCT
	route.get('/:productId', (req, res) => {
		// console.log(req.params.productId)
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});

//[SECTION] UPDATE PRODUCT INFO
	route.put('/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	});

	//ARCHIVE PRODUCT
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	})
	//ACTIVATE a product
	route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
		ProductController.activateProduct(req.params.productId).then(result => res.send(result));
	})

// //[SECTION] CREATE ORDER
// 	route.post('/create-order', verify, (req, res) => {
// 		ProductController.createOrder(req.body).then(result => res.send(result));
// 	});
	// route.post('/checkout', auth.verify, controllers.order);

//[SECTION]
module.exports = route;