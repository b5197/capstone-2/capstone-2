//[SECTION] MODULES AND DEPENDENCIES
	const mongoose = require('mongoose');

//[SECTION] SCHEMA/BLUEPRINT
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'is required']
		},
		description: {
			type: String,
			required: [true, 'is required']
		},
		price: {
			type: Number,
			required: [true, 'is required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		orders: [{
			orderId: {
				type: String,
				required: [true, 'is required']
			}
		}]
	});

//[SECTION] MODEL
	module.exports = mongoose.model('Products', productSchema);