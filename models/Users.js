//[SECTION] MODULES AND DEPENDENCIES
	const mongoose = require('mongoose');

//[SECTION] SCHEMA/BLUEPRINT
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Email is required']
		},
		password: {
			type: String,
			required: [true, 'Email is required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [{
			products: [{
				productName: {
					type: String,
					required: [true, 'is required']
				},
				quantity: {
					type: Number,
					required: [true, 'is required']
				}
			}],
			totalAmount: {
				type: Number,
				required: [true, 'is required']
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}]
	});

//[SECTION] MODEL
	module.exports = mongoose.model('User', userSchema);