//[SECTION] DEPENDENCIES AND MODULES
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');
	const cors = require('cors')

//[SECTION] ENVIRONMENT SETUP
	dotenv.config();
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT;

//[SECTION] SERVER SETUP
	const app = express();
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));
	app.use(cors());

//[SECTION] DATABASE CONNECTION
	mongoose.connect(account);
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log(`MongoDB Connected`))

//[SECTION] BACKEND ROUTES
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	


//[SECTION] SERVER GATEWAY RESPONSE
	app.get('/', (req, res) => {
		res.send(`Welcome to Passion Inside`);
	});
	app.listen(port, () => {
		console.log(`API is hosted at port ${port}`)
	});