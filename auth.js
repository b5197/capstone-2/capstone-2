//[SECTION] JWT	
	const jwt = require('jsonwebtoken');

//[SECTION] KEYWORD
	const secret = 'EcommerceAPI';

//[SECTION] CREATE TOKEN
	module.exports.createAccessToken = (user) => {
		console.log(user);
		const data = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		};
		return jwt.sign(data, secret, {})
	};

//[SECTION] TOKEN VERIFICATION
	module.exports.verify = (req, res, next) => {
		console.log(req.headers.authorization)
		let token = req.headers.authorization
		if (typeof token === 'undefined') {
			return res.send({auth: "Failed. No token"})
		} else {
			console.log(token);
			token = token.slice(7, token.length)
			jwt.verify(token, secret, function(err, decodedToken) {
				if (err) {
					return res.send({
						auth: "Failed",
						message: err.message
					})
				} else {
					console.log(decodedToken)
					req.user = decodedToken;
					next();
				};
			});
		};
	};

	//ADMIN VERIFICATION
	module.exports.verifyAdmin = (req, res, next) => {
		if (req.user.isAdmin) {
			next();
		} else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden"
			})
		}
	};