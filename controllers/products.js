//[SECTION] DEPENDENCIES AND MODULES
	const Product = require('../models/Products');
	const User = require('../models/Users');

//[SECTION] CREATE PRODUCT
	module.exports.addProduct = (reqBody) => {
		let newCourse = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newCourse.save().then((course, err ) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		}).catch(err => err)
	};

	//get all products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	//get all ACTIVE products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		}).catch(err => err)
	};

	//get SPECIFIC products
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams).then(result => {
			return result;
		}).catch(err => err)
	};

//[SECTION] UPDATE PRODUCT
	module.exports.updateProduct = (productId, data) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price
		}
		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	};

//[SECTION] ARCHIVE PRODUCT
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

	//Activating a product
	module.exports.activateProduct = (productId) => {
		let updateActiveField = {
			isActive: true
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

//[SECTION] CREATE ORDER
	// module.exports.order = async () => {
	// 	if(req.user.isAdmin) {
	// 		return res.send({ message: "Action Forbidden"})
	// 	};

	// let isUserUpdated = await User.findById(req.user.id).then(user => {
	// 		let newOrder = {
	// 			productId: req.body.productId
	// 		}
	// 		user.orders.push(newOrder)
	// 		return user.save().then(user => true).catch(err => err.message);

	// 		if (isUserUpdated !== true) {
	// 			return res.send({message: isUserUpdated})
	// 		}
	// 	})
	// //find courseId
		// let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		// 	let userOrder = {
		// 		userId: req.user.id
		// 	}
		// 	product.order.push(userOrder);
		// 	return product.save().then(product => true).catch(err => err.message)
		// 	if (isProductUpdated !== true) {
		// 		return res.send({message: isProductUpdated})
		// 	}
		// })
	// //send msg to the client
	// 	if (isUserUpdated && isProductUpdated) {
	// 		return res.send({message: "Added to your order"})
	// 	}
	// };


	// module.exports.createOrder = (reqBody) => {
	// 	let newOrder = new Order({
	// 		// name: reqBody.name,
	// 		// description: reqBody.description,
	// 		// price: reqBody.price
	// 		orderId: reqBody.orderId
	// 	});
	// 	return newOrder.save().then((course, err ) => {
	// 		if (err) {
	// 			return false;
	// 		} else {
	// 			return true;
	// 		};
	// 	}).catch(err => err)
	// };
