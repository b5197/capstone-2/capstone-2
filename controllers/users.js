//[SECTION] DEPENDENCIES AND MODULES
	const User = require('../models/Users');
	const Product = require('../models/Products');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js')

//[SECTION] ENVIRONMENT VARIABLES SETUP
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] CREATE USER
	module.exports.userReg = (userInput) => {
		let email = userInput.email;
		let password = userInput.password;

		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(password, salt)
		});
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return err;
			};
		});
	};

	//USER AUTH
	module.exports.loginUser = (data) => {
		return User.findOne({email:data.email}).then(result => {
			if (result === null) {
				return false;
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				} else {
					return false;
				};
			};
		});
	};

//[SECTION] GET/RETRIEVE
	module.exports.getProfile = data => {
		return User.findById(data).then(result => {
			result.password = '';
			return result;
		});
	};

//[SECTION] CREATE ORDER
	module.exports.createOrder = async (req,res) => {
		// console.log("Test Order Route");
		// console.log(req.user.id);
		// console.log(req.body.productId);
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}

	//get the user's ID to save the productId inside the orders field
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			let uOrder = {
				products: {
					productName: req.body.productName,
					quantity: req.body.quantity
				},
				totalAmount: req.body.totalAmount
			}
			user.orders.push(uOrder)
			return user.save().then(user => true).catch(err => err.message);

			if (isUserUpdated !== true) {
				return res.send({message: isUserUpdated})
			}
		})
	//find courseId
		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
			let pOrder = {
				orderId: req.user.id
			}
			product.orders.push(pOrder);
			return product.save().then(product => true).catch(err => err.message)
			if (isProductUpdated !== true) {
				return res.send({message: isProductUpdated})
			}
		})
	//send msg to the client
		if (isUserUpdated && isProductUpdated) {
			return res.send({message: "Added to your order!"})
		}
	};

//GET ORDERS
	module.exports.orderHistory = (req, res) => {
			User.findById(req.user.id) 
			.then(result => res.send(result.orders))
			.catch(err => res.send(err))
	} 
